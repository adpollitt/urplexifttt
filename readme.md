Simple container to take in a Webhook event from Plex (currently only media.play) and send it to an IFTTT event

 **Port** - Port the server listens for the Webhhook event from Plex. (Both PORT variables need to match)
 
 **UUID** - UUID of the device that should trigger the event (this is passed in the webhook JSON)

 **IFTKEY** - The Key from your IFTTT webhook url

 **IFTEVENT** - The name of the event set in your Webhook trigger in IFTTT



Potential future changes:

- give control over event trigger and potentially allow multiple events trigger IFTTT
- allow "All" option and multiple UUID support
- Make only one PORT variable needed
- add a mode to print the UUID of events live if no UUID is passed