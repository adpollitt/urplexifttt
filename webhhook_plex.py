from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse, parse_qs
import json
import datetime
import sys
import requests
import os

picfile = '/home/webhook_notification.jpg'
errorfile = '/home/webhook_error.txt'
try:
    PORT = int(os.getenv('PORT'))
except Exception as error:
    sys.exit("PORT environment variable missing.")
try:
    muuid = os.getenv('UUID')
except Exception as error:
    sys.exit("UUID environment variable missing.")
try:
    iftkey = os.getenv('IFTKEY')
except Exception as error:
    sys.exit("IFTKEY environment variable missing.")
try:
    iftevent = os.getenv('IFTEVENT')
except Exception as error:
    sys.exit("IFTEVENT environment variable missing.")

ifttturl = "https://maker.ifttt.com/trigger/"+iftevent+"/with/key/"+iftkey

print("IFTTT event url set to: "+ifttturl)

def stripParseQS(qs):
    try:
        return parse_qs(qs.split(b'Content-Type: image/jpeg')[0])
    except UnicodeDecodeError as uerror:
        oridinal = uerror.args[2]
        oridinalp = uerror.args[2] + 1
        return stripParseQS(qs[:oridinal] + qs[oridinalp:])

class WebhookHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        content_length = int(self.headers['content-length']) - 1
        querystring = self.rfile.read(content_length)
        data = stripParseQS(querystring)
        urldataobjects = []
        for key, value in data.items():
           urldataobjects.append(value[0])
        
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
        jdata = ""
        pic = ""
        for index in range(len(urldataobjects)):
            if "Content-Type: application/json" in urldataobjects[index].decode("utf-8"):
                jdata = urldataobjects[index]
        
        jdata = jdata.decode("utf-8").split('Content-Type: application/json')[1]
        jdata = jdata.split('------------------------------')[0]
        jdata = jdata.strip()
        try:
            while jdata[-1] != "}":
                jdata = jdata[:-1]
            jdata = json.loads(jdata)
            event = jdata["event"]
            account = jdata["Account"]["title"]
            device = jdata["Player"]["title"]
            uuid = jdata["Player"]["uuid"]
            if uuid == muuid and event == "media.play":
                url = ifttturl
                myobj = { "value1" : "", "value2" : "", "value3" : "" } 
                x = requests.post(url, data = myobj)
        except Exception as error:
            print(error)
            now = datetime.datetime.now()
            f = open(errorfile, 'a')
            f.write("[")
            f.write(now.__str__())
            f.write("]")
            f.write(" ")
            f.write('caught this error: ')
            f.write(repr(error))
            f.write("\n\n")
            f.write(json.dumps(jdata))
            f.write("\n\n\n")
            f.close()


def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    httpd = HTTPServer(('', PORT), WebhookHandler)
    httpd.serve_forever()

run()
