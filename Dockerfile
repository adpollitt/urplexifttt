FROM archlinux
RUN pacman -Syu python python-pip nano --noconfirm --noprogressbar
COPY . /home
RUN python -m pip install requests
CMD python -u /home/webhhook_plex.py